import * as router from 'aws-lambda-router'
import * as serverApi from './routes/server'



export const handler = router.handler({
  proxyIntegration: {
    cors: true,
    debug: process.env.ROUTER_DEBUG === 'true',
    routes: [ {
      path: '/api/server/ping',
      method: 'GET',
      action: (request, context) => {
        return JSON.stringify({ message: 'pong' })
      }
    },
    serverApi.get,
    serverApi.post
    ],
    errorMapping: {
      'BadRequest': 400,
      'NotFound': 404,
      'MyCustomError': 429,
      'ServerError': 500
    }
  }
})
