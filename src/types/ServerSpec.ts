export type ServerSpec = {
  host: string// -> instance hostname ie sv.netquake.io
  path: string, // /ffa or /dm etc
  portStart: number //  26010
  startupCfg: string
  gameDir: string
  shareware: boolean

  maxPlayers: number
  startMap: string
}