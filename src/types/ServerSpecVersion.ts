import { ServerSpec } from "./ServerSpec";

export type ServerSpecVersion = {
  version: number
  spec: ServerSpec
}