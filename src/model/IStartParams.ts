export interface IStartParams {
    timeLimit: number;
    fragLimit: number;
    mod: string;
    map: string;
}