export interface IServerSpec {
  serverName: string// -> instance hostname
  portStart: number //  26010
  startupCfg: string
  maxPlayer: number;
  gameDir: string
  shareware: boolean
  
  // - hostname
  // - ports (udp range and tcp)
  // - web connect host (sv.netquake.io)
  // - docker host (for control plane) 
  // - startup cfg
  // - max players
  // - game type
  // - Shareware vs REgistered?
}
