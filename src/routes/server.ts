import * as store from '../store'
import * as Joi from 'joi'

const addServerSchema = Joi.object({
  serverId: Joi.string().required(),
  name: Joi.string().required(),
  game: Joi.string().required(),
  gameType: Joi.string(),
  connecthostport: Joi.string().required(),
  location: Joi.string().required(),
  description: Joi.string()
})

export const get = {
  // request-path-pattern with a path variable:
  path: '/api/manage/server',
  method: 'GET',
  // we can use the path param 'id' in the action call:
  action: (request, context) => {
    return store.getAll()
      .catch((err) => {
        throw {reason: 'ServerError', message: JSON.stringify({message: 'Failed to add server: ' + err.message})} 
      }) as any
  }
}
export const post = {
  path: '/api/manage/server',
  method: 'POST',
  // we can use the path param 'id' in the action call:
  action: (request, context) => {
    const body = request.body
    return store.addServerSpec(body.host, body.path, body);
  }
}