import IServerDef from './IServerDef'
import IPlayerStatus from './IPlayerStatus'

export default interface IServerStatus {
  connecthostport: string,
  lastQuery: number, // Date - ms since epoch
  failedQueries: number,
  players: IPlayerStatus[],
  map: string,
  serverSettings: string,
  maxPlayers: number,
  version: number
}