import type { IStartParams } from "../model/IStartParams";
import {Docker} from 'node-docker-api'
import type { Stream } from "stream";

const getDocker = () => new Docker({ host: 'localhost', port: 2375 })
const image = 'public.ecr.aws/v9s4j9b0/webquake-server'

const promisifyStream = (stream: Stream) => new Promise((resolve, reject) => {
  stream.on('data', (d) => console.log(d.toString()))
  stream.on('end', resolve)
  stream.on('error', reject)
})

const pull = (image: string, docker: Docker) => {
  return docker.image.create({}, { fromImage: image, tag: 'latest' })
    .then((stream: Object) => promisifyStream(stream as Stream))
}

export const start = (_params: IStartParams) => {
    const docker = getDocker()
    return pull(image, docker)
      .then(() => {
        return docker.container.create({
            Image: image + ":latest",
            HostConfig: {
              Mounts: [{
                Source: 'C:/games/quake/id1',
                Target: '/usr/app/id1',
                Type: 'bind'
              }],
            },
            Ports: [{
              PrivatePort: 26000,
              PublicPort: 26000,
              Type: 'udp'
            }],
            Labels: {
              'netquakeio.managed': 'true'
            },
            Command: ["-udpportstart", "26051", "-port", "26050", "+map", "e1m5", "-game", "ffa"],
            Env: [
              `STARTUP_CFG=web_description Free for all\n
              web_connect_url sv.netquake.io/ffa\n
              web_location Ohio\n
              deathmatch 3\n
              maxplayers 16\n
              hostname "WebQuake FFA"\n
              map dm3`
            ],
            name: 'test'
        })
        .then(container => container.start())
      })
      .catch(error => console.log(error));
}   


export const list = () => {
  const docker = getDocker()

  return docker.container.list({
    filters: {
      label: [
        'netquakeio.managed=true'
      ]
    }
  })
}