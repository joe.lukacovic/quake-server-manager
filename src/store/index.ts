import * as AWS from 'aws-sdk'
import {promisify} from 'util'
import { ServerSpec } from '../types/ServerSpec'
import { ServerSpecVersion } from '../types/ServerSpecVersion'

const tableName = 'server-controller'
const primaryKey = 'PK#SERVER-SPEC#'
const secondaryKey = 'SK#SERVER-SPEC#'

const docClient = new AWS.DynamoDB.DocumentClient();


export const init = (options: any): Promise<void> => {
  return Promise.resolve()
}

// export const get = (address: string) : Promise<{spec:IServerSpec, version: number} | undefined> => {
//   var params = {
//     TableName: tableName,
//     IndexName: 'Index',
//     KeyConditionExpression: 'address = :address',
//     ExpressionAttributeValues: {
//       ':address': address
//     }
//   }
//   return promisify(docClient.query.bind(docClient))(params)
//     .then(data => {
//       if (data.Items.length) {
//         return data.Items[0]
//       }
//     })
// }

export const get = (host: string, path: string) => 
  promisify(docClient.query.bind(docClient))({
    TableName: tableName,
    KeyConditionExpression: 'PK = :pk AND SK = :sk',
    ExpressionAttributeValues: {
      ':pk': primaryKey + host,
      ':sk': secondaryKey + path,
      // "ProjectionExpression": "UserId, TopScore",
      // "ScanIndexForward": false
    }
  })
  .then(result => result.Item[0])

export const remove = (host: string, path) => {
  var params = {
    TableName: tableName,
    Key: { 'PK': primaryKey + host, 'SK':  secondaryKey + path }
  }
  return promisify(docClient.delete.bind(docClient))(params)
}

export const updateServerSpec = (host: string, path: string, serverSpec: ServerSpec) => {
  return get(host, path)
    .then(result => {
      var params = {
        TableName: tableName,
        Key: {
          "PK": primaryKey + host,
          "SK": secondaryKey + path,
        },
        UpdateExpression: "set Version = :version, set serverSpec = :serverSpec",
        ExpressionAttributeValues:{
          ":serverSpec": serverSpec,
          ":version": result.version + 1
        },
        ReturnValues: "UPDATED_NEW"
      }
    
      return promisify(docClient.update.bind(docClient))(params)
    })
}

export const addServerSpec = (host: string, path: string, serverSpec: ServerSpec) : Promise<void> =>  {
  return get(host, path)
    .then(result => {
      var params = {
        TableName: tableName,
        Key: {
          "pk": primaryKey + host,
          "sk": secondaryKey + path,
        },
        UpdateExpression: "set serverSpec = :s, version = :v",
        ExpressionAttributeValues:{
          ":s": serverSpec,
          ":v": result.version + 1
        },
        ReturnValues:"UPDATED_NEW"
      }
      
      return promisify(docClient.update.bind(docClient))(params)
    })
    .catch(err => {
      var params = { TableName: tableName, Item: {
        PK: primaryKey + host,
        SK: secondaryKey + path,
        serverSpec,
        version: 1 }}
      return promisify(docClient.put.bind(docClient))(params)
    })
}

export const getAll = () : Promise<Array<ServerSpecVersion>> => {
  return promisify(docClient.scan.bind(docClient))({
    TableName: tableName
  })
    .then(data => data.Items
    .map(item => {
      return {
        serverSpec: item.serverSpec,
        version: item.version
      }
    }))
}