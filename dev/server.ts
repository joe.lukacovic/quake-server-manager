import {APIGatewayProxyEvent, APIGatewayProxyResult} from 'aws-lambda'
import express, {Request, Response} from 'express'
import {handler} from '../src'
import dotenv from 'dotenv'

dotenv.config()
const port = process.env.PORT || 8080
const app = express()

interface ModifiedRequest extends Request {
  rawBody: string
}
app.use(function (req, res, next) {
  let data = ''
  req.on('data', function (chunk) {
    data += chunk
  })
  req.on('end', function () {
    ;(req as ModifiedRequest).rawBody = data
    next()
  })
})

const multiValue = (lolwut) => {
  return Object.keys(lolwut).reduce(
    (aggr, key) => ({
      [key]: Array.isArray(lolwut[key]) ? lolwut[key][0] : lolwut[key],
      ...aggr,
    }),
    {},
  )
}

const expressToApiGW = (req: ModifiedRequest): APIGatewayProxyEvent => ({
  body: req.rawBody,
  headers: multiValue(req.headers),
  multiValueHeaders: multiValue(req.headers),
  httpMethod: req.method,
  isBase64Encoded: false,
  path: req.path,
  pathParameters: {},
  queryStringParameters: multiValue(req.query),
  multiValueQueryStringParameters: multiValue(req.query),
  stageVariables: {dev: 'something'},
  requestContext: {} as any,
  resource: 'asdf',
})

const setHeaders = (response: Response, headers: any) => {
   Object.keys(headers).forEach(headerKey => {
     response.set(headerKey, headers[headerKey])
   })
}

app.all('*', (req, res) => {
  return handler(expressToApiGW(req as ModifiedRequest), {} as any).then((result: APIGatewayProxyResult) => {
    setHeaders(res, result.headers)
    res.status(result.statusCode)
    res.write(result.body)
    res.end()
  })
})

app.listen(port, () => {
  console.log(`Dev server listening at http://localhost:${port}`)
})
